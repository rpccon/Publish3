﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncumpIndicadores.aspx.cs"  MasterPageFile="~/Site.Master" Inherits="SICRER_Web_Application.IncumpIndicadores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <!-- Styles -->
<style>
#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}							
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

<!-- Chart code -->
        <style>
        @media only screen and (max-width: 768px) {
             .form-inline {
                display: block;
                margin-top: 10px;
                margin-bottom:10px;
            }
            .rbEspacio input[type="radio"]
            {
               margin-left: 40px;
               margin-right: 1px;
            }
            .paraDiv
            {
                border-left-style: solid;
            }
            .btn-primary btn-default{
                margin-top: 10px;
                margin-bottom:5px;
            }
            .form-control{
                margin-bottom:5px;
            }
        }
    </style>
<script>

    function generarGraphic(num, ano,mes,rest,numFiltro) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            url: 'http://' + '172.24.40.76' + '/serverStat/Datos2/' + num + '/' + ano + '/' + mes + '/' + rest + '/' + numFiltro,
            success:
                function OnGetMemberSuccess(result) {
                    console.log(result);
                    if (result.length===0) {
                        alert('Según los datos seleccionados no hay información para visualizar en el gráfico');
                    }
                    else {
                        var chart = AmCharts.makeChart("chartdiv", {
                            "type": "pie",
                            "theme": "light",
                            "innerRadius": "40%",
                            "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
                            "dataProvider": result,
                            "balloonText": "[[value]]",
                            "valueField": "litres",
                            "titleField": "country",
                            "balloon": {
                                "drop": true,
                                "adjustBorderColor": false,
                                "color": "#FFFFFF",
                                "fontSize": 30
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    }




                },
            error: function OnGetMemberSuccess(data, status) {
                alert('Error de conexión con el servidor');
            },
        });
    }



</script>


        <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li><a href="CumpIndicadores.aspx">Cumplimiento de indicadores</a></li>
            <li class="active"><a href="IncumpIndicadores.aspx.aspx">Incumplimiento de indicadores</a></li>
            <li><a href="AlasCumpResis.aspx">Cumplimiento de residencias por alas</a></li>
            <li><a href="CuartosDB.aspx">Cuartos buenos y deficientes</a></li>
            <li><a href="LlamadasAtencion.aspx">Registros llamadas de atención</a></li>
        </ul>
    </div>
      
    <div class="col-md-9 space-up" align="center">
             <asp:Panel ID="p_msjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel  ID="p_msjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>
        <ul class="nav nav-pills nav-stacked well">
                
            <div   class="form-inline">
        
                <asp:DropDownList  runat="server" ID="dd_meses" DataTextField="MES" Width="180px"  CssClass="selectpicker form-control" OnSelectedIndexChanged="dd_meses_click" DataValueField="MES"  AutoPostBack="true">
                </asp:DropDownList>        

                <asp:DropDownList  runat="server" ID="II_periodos" DataTextField="Periodo" Width="180px" OnSelectedIndexChanged="II_pers_click" CssClass="selectpicker form-control" DataValueField="Periodo"  AutoPostBack="true">
                </asp:DropDownList>


                <asp:RadioButtonList ID="rest"  ValidationGroup="valGroup1" OnSelectedIndexChanged="tipo_filtro_click" runat="server" CssClass="rbEspacio radio" RepeatDirection="Horizontal" AutoPostBack="true">
                     <asp:ListItem Value="Semestral" Selected="True"/>
                     <asp:ListItem Value="Mensual"  />
                </asp:RadioButtonList>

                    <asp:RadioButtonList ID="rb_cc"  ValidationGroup="valGroup1" runat="server" CssClass="rbEspacio radio" OnSelectedIndexChanged="changeFilter" RepeatDirection="Horizontal" AutoPostBack="true">
                         <asp:ListItem Value="Cuarto" Selected="True"/>
                         <asp:ListItem Value="Cocina"  />
                    </asp:RadioButtonList>






                 
            </div>        
        </ul>
    </div>

        



    <div class="col-md-9 space-up">
        <div id="chartdiv"></div>

    </div>


</asp:Content>

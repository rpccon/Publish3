﻿function Selectallcheckbox(val) {          
    if (!$(this).is(':checked')) {
        $('input:checkbox').prop('checked', val.checked);
    } else {
        $("#chkroot").removeAttr('checked');
    }
} 

function ShowPopupEstudiantes() {
    $('#modalEst').modal();
}

function mostrarEst() {
    $('#capaEstudiante').show();
    $('#MainContent_btnMostrarObs').html("<span class='glyphicon glyphicon-chevron-right'></span> Observaciones");
    $('#MainContent_btnMostrarEst').html("<span class='glyphicon glyphicon-chevron-down'></span> Estudiantes");
    $('#capaObservaciones').hide();
}
function mostrarObs() {
    $('#capaObservaciones').show();
    $('#MainContent_btnMostrarEst').html("<span class='glyphicon glyphicon-chevron-right'></span> Estudiantes");
    $('#MainContent_btnMostrarObs').html("<span class='glyphicon glyphicon-chevron-down'></span> Observaciones");
    $('#capaEstudiante').hide();
}

function loading(ele) {
    $('#' + ele).css("display", "inline-block");
}

function loadingPDF(ele1, ele2) {
    $('#' + ele1).css("display", "inline-block");
    if (ele2 !== "") {
        $('#' + ele2).css("display", "none");
    }
}

function loadEditar(id_accion, id_mostralPanel, id_ocultarOpcion, id_msj, tipo_accion) {
    $('#' + id_accion).css("display", "block");
    $('#' + id_mostralPanel).css("display", "inline-block");
    $('#' + id_ocultarOpcion).css("display", "none");
    if (tipo_accion === '1') {
        $('#' + id_msj).html("¿Está seguro de actualizar esta revisión?");
    }
    else {
        $('#' + id_msj).html("¿Está seguro de eliminar esta revisión?");
    }
}

function mostrarFormRegistrarInd() {

    this.formRegistrarInd.style.display = "block";

}
function ocultarFormRegistrarInd() {

    this.formRegistrarInd.style.display = "none";

}

function ocultarElemento(ele) {
    $('#' + ele).hide();
}

function cerrar() {
    window.close();
}

